from django.http import HttpResponse
from django.contrib.auth.mixins import LoginRequiredMixin
from django_filters.views import FilterView
from django.views.generic import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Expense
from .filters import ExpenseFilter
from django.urls import reverse_lazy
from django.utils import timezone
from .forms import ExpenseForm, FilterForm
import datetime
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pytz
import calendar
from django.template import loader
from django.shortcuts import render, redirect
plt.rcParams['font.family'] = 'IPAPGothic'    #日本語の文字化け防止

class ExpenseFilterView(LoginRequiredMixin, FilterView):
    model = Expense

    # デフォルトの並び順を新しい順とする
    queryset = Expense.objects.all()#.order_by('-created_at')

    # django-filter用設定
    filterset_class = ExpenseFilter
    strict = False

    # 1ページあたりの表示件数
    paginate_by = 10

    # 検索条件をセッションに保存する
    def get(self, request, **kwargs):
        if request.GET:
            request.session['query'] = request.GET
        else:
            request.GET = request.GET.copy()
            if 'query' in request.session.keys():
                for key in request.session['query'].keys():
                    request.GET[key] = request.session['query'][key]

        return super().get(request, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['chart_labels']= "'選択肢8', '選択肢2', '選択肢３', '選択肢４','選択肢５'",
        context['chart_data']= "8, 5, 6, 5, 6",
        context['chart_title']= "レーダーサンプル",
        context['chart_target']="Ａさん"
        return context



# 詳細画面
class ExpenseDetailView(LoginRequiredMixin, DetailView):
    model = Expense


# 登録画面
class ExpenseCreateView(LoginRequiredMixin, CreateView):
    model = Expense
    form_class = ExpenseForm
    success_url = reverse_lazy('index')


# 更新画面
class ExpenseUpdateView(LoginRequiredMixin, UpdateView):
    model = Expense
    form_class = ExpenseForm
    success_url = reverse_lazy('index')


# 削除画面
class ExpenseDeleteView(LoginRequiredMixin, DeleteView):
    model = Expense
    success_url = reverse_lazy('index')

TODAY = str(timezone.now()).split('-')

def graph(request, year=TODAY[0], month=TODAY[1]):
    #print(request)
    if request.method == 'GET':
        if not("year" in request.session.keys()):
            cost, cost_accumulate = calc_cost(year, month)
            form = FilterForm(None)
        else:
            year = request.session["year"]
            month = request.session["month"]
            cost = request.session["cost"]
            cost_accumulate = request.session["cost_accumulate"]
            form = FilterForm(request.session.get('form_data'))
    # 送信ボタンが押された時(POSTされた時)
    else:
        # POST
        form = FilterForm(request.POST)
        # 送信された値が正しかった時の処理
        if form.is_valid():
            # 保存
            #form.save()
            # セッションにデータを格納
            year = request.POST['year']
            month = request.POST['month']
            cost, cost_accumulate = calc_cost(year, month)
            request.session['form_data'] = request.POST
            # 遷移させるページ
            request.session["year"] = year
            request.session["month"] = month
            request.session["cost"] = cost
            request.session["cost_accumulate"] = cost_accumulate
            context = {
                "year":year,
                "month":month,
                "data":cost,
                "data_accumulate": cost_accumulate,
                'form': form
            }
            return render(request, 'tracker/graphs.html', context)
    # コンテキストにフォームのオブジェクトを指定してレンダリング

    context = {
        "year":year,
        "month":month,
        "data":cost,
        "data_accumulate": cost_accumulate,
        'form': form
    }
    return render(request, 'tracker/graphs.html', context)

def calc_cost(year, month):
    expense = Expense.objects.filter(use_date__year=year,use_date__month=month).order_by('use_date')
    last_day = calendar.monthrange(int(year), int(month))[1] + 1
    day = [i for i in range(1, last_day)]
    cost = [0 for i in range(len(day))]
    cost_accumulate = [0 for i in range(len(day))]
    sum_value = 0
    for m in expense:
        cost[int(str(m.use_date).split('-')[2].split(' ')[0])-1] += int(m.expense)
    cost_accumulate[0] = cost[0]
    for i in range(len(cost)-1):
        cost_accumulate[i+1] = cost[i+1]+cost_accumulate[i]
    return cost, cost_accumulate

"""
def graph(request, year=TODAY[0], month=TODAY[1]):
    expense = Expense.objects.filter(use_date__year=year,use_date__month=month).order_by('use_date')
    last_day = calendar.monthrange(int(year), int(month))[1] + 1
    day = [i for i in range(1, last_day)]
    cost = [0 for i in range(len(day))]
    cost_accumulate = [0 for i in range(len(day))]
    sum_value = 0
    for m in expense:
        cost[int(str(m.use_date).split('-')[2].split(' ')[0])-1] += int(m.expense)
    cost_accumulate[0] = cost[0]
    for i in range(len(cost)-1):
        cost_accumulate[i+1] = cost[i+1]+cost_accumulate[i]
    plt.figure()
    plt.bar(day, cost, color='#00bfff', edgecolor='#0000ff')
    plt.grid(True)
    plt.xlim([0, 31])
    plt.xlabel('日付', fontsize=16)
    plt.ylabel('支出額(円)', fontsize=16)
    #staticフォルダの中にimagesというフォルダを用意しておきその中に入るようにしておく
    plt.savefig('tracker/static/tracker/images/bar_{}_{}.svg'.format(year, month),
            transparent=True)
    context = {
        "year":year,
        "month":month,
        "data":cost,
        "data_accumulate": cost_accumulate,
    }
    return render(request, 'tracker/graphs.html', context)
"""