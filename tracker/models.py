from django.db import models

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=15)
    class Meta:
        verbose_name = 'カテゴリー'
        verbose_name_plural = 'カテゴリー'
    def __str__(self):
        return self.name


class Expense(models.Model):
    name = models.CharField(max_length=15)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null = True)
    expense = models.IntegerField(default=0)
    use_date = models.DateTimeField('Expense Date')
    class Meta:
        verbose_name = '使用費用'
        verbose_name_plural = '使用費用'