from django_filters import FilterSet
from django_filters.widgets import RangeWidget
from django_filters import filters

from .models import Expense


class MyOrderingFilter(filters.OrderingFilter):
    descending_fmt = '%s （降順）'


class ExpenseFilter(FilterSet):
    #category = filters.CharFilter(label='名前', lookup_expr='contains')
    #use_date = filters.DateFromToRangeFilter()
    class Meta:
        model = Expense
        # フィルタを列挙する。
        # デフォルトの検索方法でいいなら、モデルフィールド名のフィルタを直接定義できる。
        fields = '__all__'