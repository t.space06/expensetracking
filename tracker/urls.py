from django.urls import path

from . import views
from .views import ExpenseFilterView, ExpenseDetailView, ExpenseCreateView, ExpenseUpdateView, ExpenseDeleteView

urlpatterns = [
    path('', ExpenseFilterView.as_view(), name='index'),
    path('detail/<int:pk>/', ExpenseDetailView.as_view(), name='detail'),
    path('create/', ExpenseCreateView.as_view(), name='create'),
    path('update/<int:pk>/', ExpenseUpdateView.as_view(), name='update'),
    path('delete/<int:pk>/', ExpenseDeleteView.as_view(), name='delete'),
    path('graph/', views.graph, name='graph'),
]