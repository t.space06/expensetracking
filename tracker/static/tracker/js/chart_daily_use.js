var ctx = document.getElementById("graph");
var myLineChart = new Chart(ctx, {
  type: "line",
  data: {
    labels: [...Array(31)].map((_, i) => i + 1), //=> [ 1, 2, 3, 4, 5 ]
    datasets: [
      {
        label:'使用金額 [円]',
        data: data,  // ここで data を使っている
        borderColor: "salmon",
        backgroundColor: "rgba(0,0,0,0)",
      },
    ],
  },
  options: {
    scales: {
      xAxes: [{
        type: 'linear',
        position: 'bottom',
        ticks: {
          callback: function(value) {return ((value % 10) == 0)? value : ''},
          min: 0,
          max: 50,
          stepSize: 1
        }
      }],
      yAxes: [{
        ticks: {
          callback: function(value) {return ((value % 10) == 0)? value : ''},
          min: 0,
          max: 50,
          stepSize: 1
        }
      }]
    }
  }
});