from django import forms
from .models import Expense
from django.core.validators import MaxValueValidator, MinValueValidator



class ExpenseForm(forms.ModelForm):

    class Meta:
        model = Expense
        
        fields = ('name','expense', 'category', 'use_date')
        widgets = {
                    'name': forms.TextInput(attrs={'placeholder':'記入例：食費'}),
                    'expense': forms.NumberInput(attrs={'min':1}),
                    'use_date': forms.DateInput(attrs={"type":"date"})
                  }

class FilterForm(forms.Form):
    year = forms.IntegerField(validators=[MinValueValidator(1900), MaxValueValidator(3000)])
    month = forms.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(12)])